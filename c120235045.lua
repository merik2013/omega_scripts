local m=120235045
local list={120105001}
local cm=_G["c"..m]
cm.name="大道武器-破刃剑"
function cm.initial_effect(c)
	RD.AddCodeList(c,list)
	--Activate
	RD.RegisterEquipEffect(c,cm.condition,nil,cm.target)
	--Atk Up
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_EQUIP)
	e1:SetCode(EFFECT_UPDATE_ATTACK)
	e1:SetValue(cm.upval)
	c:RegisterEffect(e1)
	--Indes
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_EQUIP)
	e2:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
	e2:SetValue(cm.indval)
	c:RegisterEffect(e2)
end
--Activate
function cm.confilter(c)
	return c:IsFaceup() and c:IsCode(m)
end
function cm.condition(e,tp,eg,ep,ev,re,r,rp)
	return not Duel.IsExistingMatchingCard(cm.confilter,tp,LOCATION_ONFIELD,0,1,e:GetHandler())
end
function cm.target(c,e,tp)
	return c:IsControler(tp) and c:IsFaceup() and c:IsCode(list[1])
end
--Atk Up
function cm.upfilter(c)
	return c:IsFaceup() and c:IsType(TYPE_EQUIP) and c:GetEquipTarget()~=nil
end
function cm.upval(e,c)
	return Duel.GetMatchingGroupCount(cm.upfilter,0,LOCATION_SZONE,LOCATION_SZONE,nil)*1000
end
--Indes
cm.indval=RD.ValueEffectIndesType(0,TYPE_TRAP,true)