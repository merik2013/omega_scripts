local m=120222038
local cm=_G["c"..m]
cm.name="水稻梯田驱除者"
function cm.initial_effect(c)
	--Discard Deck
	local e1=Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(m,0))
	e1:SetCategory(CATEGORY_DECKDES+CATEGORY_POSITION)
	e1:SetType(EFFECT_TYPE_IGNITION)
	e1:SetRange(LOCATION_MZONE)
	e1:SetCondition(cm.condition)
	e1:SetTarget(cm.target)
	e1:SetOperation(cm.operation)
	c:RegisterEffect(e1)
end
--Discard Deck
function cm.exfilter(c)
	return c:GetSequence()<5
end
function cm.posfilter(c)
	return c:IsFaceup() and c:IsType(TYPE_NORMAL) and RD.IsCanChangePosition(c) and c:IsCanTurnSet()
end
function cm.condition(e,tp,eg,ep,ev,re,r,rp)
	return RD.IsSummonTurn(e:GetHandler())
end
function cm.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsPlayerCanDiscardDeck(tp,2) end
	Duel.SetOperationInfo(0,CATEGORY_DECKDES,nil,0,tp,2)
end
function cm.operation(e,tp,eg,ep,ev,re,r,rp)
	if RD.SendDeckTopToGraveAndExists(tp,2)
		and Duel.IsExistingMatchingCard(cm.exfilter,tp,0,LOCATION_SZONE,1,nil) then
		RD.CanSelectAndDoAction(aux.Stringid(m,1),HINTMSG_POSCHANGE,cm.posfilter,tp,0,LOCATION_MZONE,1,1,nil,function(g)
			RD.ChangePosition(g,POS_FACEDOWN_DEFENSE)
		end)
	end
end