-- Rush Duel 编号
RushDuel = RushDuel or {}

LEGEND_MONSTER = 120000000
LEGEND_SPELL = 120000001
LEGEND_TRAP = 120000002
LEGEND_DARK_MAGICIAN = {120130000, 120203015}
LEGEND_BLUE_EYES_WHITE_DRAGON = {120120000, 120198001, 120230001}
LEGEND_RED_EYES_BLACK_DRAGON = {120125001, 120203016, 120229101}

-- 添加记述卡牌列表
function RushDuel.AddCodeList(card, ...)
    for _, list in ipairs {...} do
        local type = aux.GetValueType(list)
        if type == "number" then
            aux.AddCodeList(card, list)
        elseif type == "table" then
            aux.AddCodeList(card, table.unpack(list))
        end
    end
end

-- 条件: 当前卡名是否为传说卡
function RushDuel.IsLegend(card)
    return card:IsCode(LEGEND_MONSTER, LEGEND_SPELL, LEGEND_TRAP)
end

-- 条件: 是否为传说卡
function RushDuel.IsLegendCard(card)
    return card:IsHasEffect(EFFECT_LEGEND_CARD)
end

-- 条件: 是否为传说卡的卡名
function RushDuel.IsLegendCode(card, ...)
    local code = RushDuel.GetCardCode(card)
    return RushDuel.FlatCheck(function(item)
        return code == item
    end, ...)
end

-- 获取卡牌的密码
function RushDuel.GetCardCode(card)
    local code = card:GetCode()
    if RushDuel.IsLegendCard(card) then
        code = card:GetOriginalCode()
    end
    local codes = RushDuel.GetEffectValues(card, EFFECT_CHANGE_CODE)
    for _, val in ipairs(codes) do
        code = val
    end
    return code
end

-- 条件: 是否为同名卡
function RushDuel.IsSameCode(card1, card2)
    return RushDuel.GetCardCode(card1) == RushDuel.GetCardCode(card2)
end
-- 条件: 是否卡名不同
function RushDuel.IsDifferentCode(card1, card2)
    return RushDuel.GetCardCode(card1) ~= RushDuel.GetCardCode(card2)
end
