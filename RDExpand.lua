-- Expand
RushDuel = RushDuel or {}
RD = RushDuel

-- Make Card(s) or Group into a New Group (Maximum)
function RushDuel.ToMaximunGroup(target)
    local type = aux.GetValueType(target)
    local g = Group.CreateGroup()
    if type == "Group" then
        g:Merge(target)
    elseif type == "Card" then
        g:AddCard(target)
    elseif type == "Effect" then
        g:AddCard(target:GetHandler())
    end
    local overlay = Group.CreateGroup()
    g:ForEach(function(tc)
        if RushDuel.IsMaximumMode(tc) then
            overlay:Merge(tc:GetOverlayGroup())
        end
    end)
    g:Merge(overlay)
    return g
end
-- Get Useable Monster Zone Count
function RushDuel.GetMZoneCount(player, max)
    local ct = Duel.GetLocationCount(player, LOCATION_MZONE)
    if Duel.IsPlayerAffectedByEffect(player, 59822133) then
        ct = math.min(ct, 1)
    end
    return math.min(ct, max)
end
-- Get Useable Spell & Trap Zone Count
function RushDuel.GetSZoneCount(player, max)
    local ct = Duel.GetLocationCount(player, LOCATION_SZONE)
    return math.min(ct, max)
end

-- Hint (LP Label)
function RushDuel.CreateHintEffect(e, desc, tp, s, o, reset)
    local e1 = Effect.CreateEffect(e:GetHandler())
    e1:SetDescription(desc)
    e1:SetType(EFFECT_TYPE_FIELD)
    e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET + EFFECT_FLAG_CLIENT_HINT)
    e1:SetTargetRange(s, o)
    e1:SetReset(reset)
    Duel.RegisterEffect(e1, tp)
    return e1
end
-- Can not Attack (Promise)
function RushDuel.CreateAttackLimitEffect(e, target, tp, s, o, reset)
    local e1 = Effect.CreateEffect(e:GetHandler())
    e1:SetType(EFFECT_TYPE_FIELD)
    e1:SetCode(EFFECT_CANNOT_ATTACK)
    e1:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
    e1:SetTargetRange(s, o)
    if target ~= nil then
        e1:SetTarget(target)
    end
    e1:SetReset(reset)
    Duel.RegisterEffect(e1, tp)
    return e1
end
-- Can not DirectA Attack (Promise)
function RushDuel.CreateCannotDirectAttackEffect(e, target, tp, s, o, reset)
    local e1 = Effect.CreateEffect(e:GetHandler())
    e1:SetType(EFFECT_TYPE_FIELD)
    e1:SetCode(EFFECT_CANNOT_DIRECT_ATTACK)
    e1:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
    e1:SetTargetRange(s, o)
    if target ~= nil then
        e1:SetTarget(target)
    end
    e1:SetReset(reset)
    Duel.RegisterEffect(e1, tp)
    return e1
end
-- Can not Summon (Promise)
function RushDuel.CreateCannotSummonEffect(e, desc, target, tp, s, o, reset)
    local e1 = Effect.CreateEffect(e:GetHandler())
    e1:SetDescription(desc)
    e1:SetType(EFFECT_TYPE_FIELD)
    e1:SetCode(EFFECT_CANNOT_SUMMON)
    e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET + EFFECT_FLAG_CLIENT_HINT)
    e1:SetTargetRange(s, o)
    if target ~= nil then
        e1:SetTarget(target)
    end
    e1:SetReset(reset)
    Duel.RegisterEffect(e1, tp)
    return e1
end
-- Can not Special Summon (Promise)
function RushDuel.CreateCannotSpecialSummonEffect(e, desc, target, tp, s, o, reset)
    local e1 = Effect.CreateEffect(e:GetHandler())
    e1:SetDescription(desc)
    e1:SetType(EFFECT_TYPE_FIELD)
    e1:SetCode(EFFECT_CANNOT_SPECIAL_SUMMON)
    e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET + EFFECT_FLAG_CLIENT_HINT)
    e1:SetTargetRange(s, o)
    if target ~= nil then
        e1:SetTarget(target)
    end
    e1:SetReset(reset)
    Duel.RegisterEffect(e1, tp)
    return e1
end

-- Check: Is Life Point Below
function RushDuel.IsLPBelow(player, lp)
    return Duel.GetLP(player) <= lp
end
-- Check: Is Life Point Below Opponent
function RushDuel.IsLPBelowOpponent(player, lp)
    return Duel.GetLP(player) <= Duel.GetLP(1 - player) - (lp or 0)
end
-- Check: Is in Maximum Mode
function RushDuel.IsMaximumMode(c)
    return c:IsSummonType(SUMMON_TYPE_MAXIMUM) and c:GetOverlayCount() > 0
end
-- Check: Is Summon this Turn
function RushDuel.IsSummonTurn(c)
    return c:IsReason(REASON_SUMMON) and c:IsStatus(STATUS_SUMMON_TURN)
end
-- Check: Is Special Summon this Turn
function RushDuel.IsSpecialSummonTurn(c)
    return c:IsReason(REASON_SPSUMMON) and c:IsStatus(STATUS_SPSUMMON_TURN)
end
-- Check: Is Defense Above
function RushDuel.IsDefenseAbove(c, def)
    return c:IsDefenseAbove(def) and not RushDuel.IsMaximumMode(c)
end
-- Check: Is Defense Below
function RushDuel.IsDefenseBelow(c, def)
    return c:IsDefenseBelow(def) and not RushDuel.IsMaximumMode(c)
end
-- Check: Is can Change Def
function RushDuel.IsCanChangeDef(c)
    return c:IsDefenseAbove(0) and not RushDuel.IsMaximumMode(c)
end
-- Check: Is can Change Position
function RushDuel.IsCanChangePosition(c)
    return c:IsCanChangePosition() and not RushDuel.IsMaximumMode(c)
end
-- Check: Is can be Special Summoned
function RushDuel.IsCanBeSpecialSummoned(c, e, tp, pos)
    return c:IsCanBeSpecialSummoned(e, 0, tp, false, false, pos)
end
-- Check: Is can Attach Effect (Direct Attack)
function RushDuel.IsCanAttachDirectAttack(c)
    return not c:IsHasEffect(EFFECT_DIRECT_ATTACK) and not c:IsHasEffect(EFFECT_CANNOT_ATTACK) and not c:IsHasEffect(EFFECT_CANNOT_DIRECT_ATTACK)
end
-- Check: Is can Attach Effect (Pierce)
function RushDuel.IsCanAttachPierce(c)
    return not c:IsHasEffect(EFFECT_CANNOT_ATTACK)
end
-- Check: Is can Attach Effect (Double Tribute)
function RushDuel.IsCanAttachDoubleTribute(c)
    return not c:IsHasEffect(EFFECT_UNRELEASABLE_SUM)
end
-- Check: Is Operated Group Exists Card(s)
function RushDuel.IsOperatedGroupExists(filter, count, expect)
    return filter == nil or Duel.GetOperatedGroup():IsExists(filter, count, expect)
end

-- Cost: Pay LP
function RushDuel.CostPayLP(lp)
    return function(e, tp, eg, ep, ev, re, r, rp, chk)
        if chk == 0 then
            return Duel.CheckLPCost(tp, lp)
        end
        Duel.PayLPCost(tp, lp)
    end
end
-- Cost: Show Hand Card(s) to Opponent
function RushDuel.CostShowHand(filter, min, max, set_label, set_object)
    return function(e, tp, eg, ep, ev, re, r, rp, chk)
        if chk == 0 then
            return Duel.IsExistingMatchingCard(filter, tp, LOCATION_HAND, 0, min, e:GetHandler(), e, tp)
        end
        Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_CONFIRM)
        local g = Duel.SelectMatchingCard(tp, filter, tp, LOCATION_HAND, 0, min, max, e:GetHandler(), e, tp)
        if set_label ~= nil then
            e:SetLabel(set_label(g))
        end
        if set_object ~= nil then
            e:SetLabelObject(set_object(g))
        end
        Duel.ConfirmCards(1 - tp, g)
        Duel.ShuffleHand(tp)
    end
end
-- Cost: Send Deck Top Card(s) to Grave
function RushDuel.CostSendDeckTopToGrave(count)
    return function(e, tp, eg, ep, ev, re, r, rp, chk)
        if chk == 0 then
            return Duel.IsPlayerCanDiscardDeckAsCost(tp, count)
        end
        Duel.DiscardDeck(tp, count, REASON_COST)
    end
end
-- Cost: Send Card(s) to Grave
function RushDuel.CostSendToGrave(filter, field, min, max, except_self, set_label_before, set_object_before, set_label_after, set_object_after)
    return function(e, tp, eg, ep, ev, re, r, rp, chk)
        local exc = nil
        if except_self then
            exc = e:GetHandler()
        end
        if chk == 0 then
            return Duel.IsExistingMatchingCard(filter, tp, field, 0, min, exc, e, tp)
        end
        Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TOGRAVE)
        local g = Duel.SelectMatchingCard(tp, filter, tp, field, 0, min, max, exc, e, tp)
        if set_label_before ~= nil then
            e:SetLabel(set_label_before(g))
        end
        if set_object_before ~= nil then
            e:SetLabelObject(set_object_before(g))
        end
        if Duel.SendtoGrave(g, REASON_COST) ~= 0 and (set_label_after ~= nil or set_object_after ~= nil) then
            local og = Duel.GetOperatedGroup()
            if set_label_after ~= nil then
                e:SetLabel(set_label_after(g))
            end
            if set_object_after ~= nil then
                e:SetLabelObject(set_object_after(g))
            end
        end
    end
end
-- Cost: Send Hand Card(s) to Grave
function RushDuel.CostSendHandToGrave(filter, min, max, set_label_before, set_object_before, set_label_after, set_object_after)
    return RushDuel.CostSendToGrave(filter, LOCATION_HAND, min, max, true, set_label_before, set_object_before, set_label_after, set_object_after)
end
-- Cost: Send Monster Zone Card(s) to Grave
function RushDuel.CostSendMZoneToGrave(filter, min, max, except_self, set_label_before, set_object_before, set_label_after, set_object_after)
    return RushDuel.CostSendToGrave(filter, LOCATION_MZONE, min, max, except_self, set_label_before, set_object_before, set_label_after, set_object_after)
end
-- Cost: Send on Field Card(s) to Grave
function RushDuel.CostSendOnFieldToGrave(filter, min, max, except_self, set_label_before, set_object_before, set_label_after, set_object_after)
    return RushDuel.CostSendToGrave(filter, LOCATION_ONFIELD, min, max, except_self, set_label_before, set_object_before, set_label_after, set_object_after)
end
-- Cost: Send Self to Grave
function RushDuel.CostSendSelfToGrave()
    return function(e, tp, eg, ep, ev, re, r, rp, chk)
        if chk == 0 then
            return e:GetHandler():IsAbleToGraveAsCost()
        end
        Duel.SendtoGrave(RushDuel.ToMaximunGroup(e:GetHandler()), REASON_COST)
    end
end
-- Cost: Send Card(s) to Deck
function RushDuel.CostSendToDeck(filter, field, min, max, except_self, sequence, set_label_before, set_object_before, set_label_after, set_object_after)
    return function(e, tp, eg, ep, ev, re, r, rp, chk)
        local exc = nil
        if except_self then
            exc = e:GetHandler()
        end
        if chk == 0 then
            return Duel.IsExistingMatchingCard(filter, tp, field, 0, min, exc, e, tp)
        end
        Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_TODECK)
        local g = Duel.SelectMatchingCard(tp, filter, tp, field, 0, min, max, exc, e, tp)
        if set_label_before ~= nil then
            e:SetLabel(set_label_before(g))
        end
        if set_object_before ~= nil then
            e:SetLabelObject(set_object_before(g))
        end
        if sequence == 1 and g:GetCount() > 1 then
            Duel.SendtoDeck(g, nil, 0, REASON_COST)
        else
            Duel.SendtoDeck(g, nil, sequence, REASON_COST)
        end
        local og = Duel.GetOperatedGroup()
        local ct = og:FilterCount(Card.IsLocation, nil, LOCATION_DECK)
        if sequence ~= 2 and ct > 1 then
            Duel.SortDecktop(tp, tp, ct)
            if sequence == 1 then
                for i = 1, ct do
                    local tc = Duel.GetDecktopGroup(tp, 1):GetFirst()
                    Duel.MoveSequence(tc, 1)
                end
            end
        end
        if set_label_after ~= nil then
            e:SetLabel(set_label_after(g))
        end
        if set_object_after ~= nil then
            e:SetLabelObject(set_object_after(g))
        end
    end
end
-- Cost: Send Hand Card(s) to Deck
function RushDuel.CostSendHandToDeck(filter, min, max, set_label_before, set_object_before, set_label_after, set_object_after)
    return RushDuel.CostSendToDeck(filter, LOCATION_HAND, min, max, true, 2, set_label_before, set_object_before, set_label_after, set_object_after)
end
-- Cost: Send Hand Card(s) to Deck Top (Sort)
function RushDuel.CostSendHandToDeckTop(filter, min, max, set_label_before, set_object_before, set_label_after, set_object_after)
    return RushDuel.CostSendToDeck(filter, LOCATION_HAND, min, max, true, 0, set_label_before, set_object_before, set_label_after, set_object_after)
end
-- Cost: Send Hand Card(s) to Deck Bottom (Sort)
function RushDuel.CostSendHandToDeckBottom(filter, min, max, set_label_before, set_object_before, set_label_after, set_object_after)
    return RushDuel.CostSendToDeck(filter, LOCATION_HAND, min, max, true, 1, set_label_before, set_object_before, set_label_after, set_object_after)
end
-- Cost: Send Grave to Deck
function RushDuel.CostSendGraveToDeck(filter, min, max, set_label_before, set_object_before, set_label_after, set_object_after)
    return RushDuel.CostSendToDeck(filter, LOCATION_GRAVE, min, max, false, 2, set_label_before, set_object_before, set_label_after, set_object_after)
end
-- Cost: Send Grave to Deck Top (Sort)
function RushDuel.CostSendGraveToDeckTop(filter, min, max, set_label_before, set_object_before, set_label_after, set_object_after)
    return RushDuel.CostSendToDeck(filter, LOCATION_GRAVE, min, max, false, 0, set_label_before, set_object_before, set_label_after, set_object_after)
end
-- Cost: Send Grave to Deck Bottom (Sort)
function RushDuel.CostSendGraveToDeckBottom(filter, min, max, set_label_before, set_object_before, set_label_after, set_object_after)
    return RushDuel.CostSendToDeck(filter, LOCATION_GRAVE, min, max, false, 1, set_label_before, set_object_before, set_label_after, set_object_after)
end

-- Target: Draw
function RushDuel.TargetDraw(player, count)
    Duel.SetTargetPlayer(player)
    Duel.SetTargetParam(count)
    Duel.SetOperationInfo(0, CATEGORY_DRAW, nil, 0, player, count)
end
-- Target: Recover
function RushDuel.TargetRecover(player, recover)
    Duel.SetTargetPlayer(player)
    Duel.SetTargetParam(recover)
    Duel.SetOperationInfo(0, CATEGORY_RECOVER, nil, 0, player, recover)
end
-- Target: Damage
function RushDuel.TargetDamage(player, damage)
    Duel.SetTargetPlayer(player)
    Duel.SetTargetParam(damage)
    Duel.SetOperationInfo(0, CATEGORY_DAMAGE, nil, 0, player, damage)
end

-- Action: Draw
function RushDuel.Draw(player, count)
    local p, d = Duel.GetChainInfo(0, CHAININFO_TARGET_PLAYER, CHAININFO_TARGET_PARAM)
    return Duel.Draw(player or p, count or d, REASON_EFFECT)
end
-- Action: Can Draw
function RushDuel.CanDraw(desc, player, count)
    if Duel.IsPlayerCanDraw(player, count) and Duel.SelectYesNo(player, desc) then
        Duel.Draw(player, count, REASON_EFFECT)
    end
end
-- Action: Recover
function RushDuel.Recover(player, recover)
    local p, d = Duel.GetChainInfo(0, CHAININFO_TARGET_PLAYER, CHAININFO_TARGET_PARAM)
    return Duel.Recover(player or p, recover or d, REASON_EFFECT)
end
-- Action: Damage
function RushDuel.Damage(player, damage)
    local p, d = Duel.GetChainInfo(0, CHAININFO_TARGET_PLAYER, CHAININFO_TARGET_PARAM)
    return Duel.Damage(player or p, damage or d, REASON_EFFECT)
end
-- Private Action: Select Card(s) and Do Action
function RushDuel._select_and_do_action(hint, filter, tp, s, o, min, max, expect, action, optional, desc, ...)
    local condition = false
    if optional then
        condition = Duel.IsExistingMatchingCard(filter, tp, s, o, min, expect, ...) and Duel.SelectYesNo(tp, desc)
    else
        condition = min < 2 or Duel.IsExistingMatchingCard(filter, tp, s, o, min, expect, ...)
    end
    if condition then
        Duel.Hint(HINT_SELECTMSG, tp, hint)
        local g = Duel.SelectMatchingCard(tp, filter, tp, s, o, min, max, expect, ...)
        if g:GetCount() > 0 then
            Duel.HintSelection(g)
            return action(g, ...)
        end
    end
end
-- Private Action: Select Card(s) by Group Sub Check and Do Action
function RushDuel._select_group_and_do_action(hint, filter, check, tp, s, o, min, max, expect, action, optional, desc, ...)
    local g = Duel.GetMatchingGroup(filter, tp, s, o, expect, ...)
    if g:CheckSubGroup(check, min, max, ...) and (not optional or Duel.SelectYesNo(tp, desc)) then
        Duel.Hint(HINT_SELECTMSG, tp, hint)
        local sg = g:SelectSubGroup(tp, check, false, min, max, ...)
        if sg:GetCount() > 0 then
            Duel.HintSelection(sg)
            return action(sg, ...)
        end
    end
end
-- Private Action: Special Summon
function RushDuel._special_summon(g, e, tp, break_effect, pos)
    if break_effect then
        Duel.BreakEffect()
    end
    return Duel.SpecialSummon(g, 0, tp, tp, false, false, pos)
end
-- Private Action: Set Spell & Trap Card
function RushDuel._set_spell_trap(g, e, tp, break_effect)
    if break_effect then
        Duel.BreakEffect()
    end
    return Duel.SSet(tp, g)
end
-- Action: Select Card(s) and Do Action
function RushDuel.SelectAndDoAction(hint, filter, tp, s, o, min, max, expect, action)
    return RushDuel._select_and_do_action(hint, filter, tp, s, o, min, max, expect, action)
end
-- Action: Can Select Card(s) and Do Action (Select Yes No)
function RushDuel.CanSelectAndDoAction(desc, hint, filter, tp, s, o, min, max, expect, action)
    return RushDuel._select_and_do_action(hint, filter, tp, s, o, min, max, expect, action, true, desc)
end
-- Action: Select Card(s) by Group Sub Check and Do Action
function RushDuel.SelectGroupAndDoAction(hint, filter, check, tp, s, o, min, max, expect, action)
    return RushDuel._select_group_and_do_action(hint, filter, check, tp, s, o, min, max, expect, action)
end
-- Action: Can Select Card(s) by Group Sub Check and Do Action
function RushDuel.CanSelectGroupAndDoAction(desc, hint, filter, check, tp, s, o, min, max, expect, action)
    return RushDuel._select_group_and_do_action(hint, filter, check, tp, s, o, min, max, expect, action, true, desc)
end
-- Action: Select Monster Card(s) and Special Summon
function RushDuel.SelectAndSpecialSummon(filter, e, tp, s, o, min, max, expect, pos, break_effect)
    local ct = RushDuel.GetMZoneCount(tp, max)
    if ct >= min then
        return RushDuel._select_and_do_action(HINTMSG_SPSUMMON, filter, tp, s, o, min, ct, expect, RushDuel._special_summon, false, nil, e, tp, break_effect, pos)
    end
    return 0
end
-- Action: Can Select Monster Card(s) and Special Summon (Select Yes No)
function RushDuel.CanSelectAndSpecialSummon(desc, filter, e, tp, s, o, min, max, expect, pos, break_effect)
    local ct = RushDuel.GetMZoneCount(tp, max)
    if ct >= min then
        return RushDuel._select_and_do_action(HINTMSG_SPSUMMON, filter, tp, s, o, min, ct, expect, RushDuel._special_summon, true, desc, e, tp, break_effect, pos)
    end
    return 0
end
-- Action: Select Spell and Trap Card(s) and Set
function RushDuel.SelectAndSet(filter, e, tp, s, o, min, max, expect, break_effect)
    local ct = RushDuel.GetSZoneCount(tp, max)
    if ct >= min then
        return RushDuel._select_and_do_action(HINTMSG_SET, filter, tp, s, o, min, ct, expect, RushDuel._set_spell_trap, false, nil, e, tp, break_effect)
    end
    return 0
end
-- Action: Can Select Spell and Trap Card(s) and Set (Select Yes No)
function RushDuel.CanSelectAndSet(desc, filter, e, tp, s, o, min, max, expect, break_effect)
    local ct = RushDuel.GetSZoneCount(tp, max)
    if ct >= min then
        return RushDuel._select_and_do_action(HINTMSG_SET, filter, tp, s, o, min, ct, expect, RushDuel._set_spell_trap, true, desc, e, tp, break_effect)
    end
    return 0
end
-- Action: Select Spell and Trap Card(s) by Group Sub Check and Set
function RushDuel.CanSelectGroupAndSet(filter, check, e, tp, s, o, min, max, expect, break_effect)
    local ct = RushDuel.GetSZoneCount(tp, max)
    if ct >= min then
        return RushDuel._select_group_and_do_action(HINTMSG_SET, filter, check, tp, s, o, min, ct, expect, RushDuel._set_spell_trap, false, nil, e, tp, break_effect)
    end
    return 0
end
-- Action: Can Select Spell and Trap Card(s) by Group Sub Check and Set (Select Yes No)
function RushDuel.CanSelectGroupAndSet(desc, filter, check, e, tp, s, o, min, max, expect, break_effect)
    local ct = RushDuel.GetSZoneCount(tp, max)
    if ct >= min then
        return RushDuel._select_group_and_do_action(HINTMSG_SET, filter, check, tp, s, o, min, ct, expect, RushDuel._set_spell_trap, true, desc, e, tp, break_effect)
    end
    return 0
end
-- Action: Attach a Buff Effect
function RushDuel.AttachSingleEffect(e, c, code, value, desc, reset)
    local e1 = Effect.CreateEffect(e:GetHandler())
    e1:SetType(EFFECT_TYPE_SINGLE)
    e1:SetCode(code)
    if code ~= nil then
        e1:SetValue(value)
    end
    if desc ~= nil then
        e1:SetDescription(desc)
        e1:SetProperty(EFFECT_FLAG_CLIENT_HINT)
    end
    if reset ~= nil then
        e1:SetReset(reset)
    end
    c:RegisterEffect(e1)
    return e1
end
-- Action: Attach Atk and Def Up/Down Buff
function RushDuel.AttachAtkDef(e, c, atk, def, reset)
    if atk ~= nil and atk ~= 0 then
        RushDuel.AttachSingleEffect(e, c, EFFECT_UPDATE_ATTACK, atk, nil, reset)
    end
    if def ~= nil and def ~= 0 and RushDuel.IsCanChangeDef(c) then
        RushDuel.AttachSingleEffect(e, c, EFFECT_UPDATE_DEFENSE, def, nil, reset)
    end
end
-- Action: Attach Level Up/Down Buff
function RushDuel.AttachLevel(e, c, lv, reset)
    return RushDuel.AttachSingleEffect(e, c, EFFECT_UPDATE_LEVEL, lv, nil, reset)
end
-- Action: Attach Direct Attack
function RushDuel.AttachDirectAttack(e, c, desc, reset)
    return RushDuel.AttachSingleEffect(e, c, EFFECT_DIRECT_ATTACK, nil, desc, reset)
end
-- Action: Attach Pierce
function RushDuel.AttachPierce(e, c, desc, reset)
    return RushDuel.AttachSingleEffect(e, c, EFFECT_PIERCE, nil, desc, reset)
end
-- Action: Attach Extra Attack
function RushDuel.AttachExtraAttack(e, c, value, desc, reset)
    return RushDuel.AttachSingleEffect(e, c, EFFECT_EXTRA_ATTACK, value, desc, reset)
end
-- Action: Attach Attack All
function RushDuel.AttachAttackAll(e, c, value, desc, reset)
    return RushDuel.AttachSingleEffect(e, c, EFFECT_ATTACK_ALL, value, desc, reset)
end
-- Action: Attach Double Tribute
function RushDuel.AttachDoubleTribute(e, c, value, desc, reset)
    return RushDuel.AttachSingleEffect(e, c, EFFECT_DOUBLE_TRIBUTE, value, desc, reset)
end
-- Action: Attach Effect Indestructible
function RushDuel.AttachEffectIndes(e, c, value, desc, reset)
    return RushDuel.AttachSingleEffect(e, c, EFFECT_INDESTRUCTABLE_EFFECT, value, desc, reset)
end
-- Action: Attach Attack Announce Buff
function RushDuel.AttachAttackAnnounce(e, c, operation, desc, reset)
    local e1 = Effect.CreateEffect(e:GetHandler())
    e1:SetType(EFFECT_TYPE_SINGLE + EFFECT_TYPE_CONTINUOUS)
    e1:SetCode(EVENT_ATTACK_ANNOUNCE)
    e1:SetOperation(operation)
    if desc ~= nil then
        e1:SetDescription(desc)
        e1:SetProperty(EFFECT_FLAG_CLIENT_HINT)
    end
    e1:SetReset(reset)
    c:RegisterEffect(e1)
end
-- Action: Attach Cannot Direct Attack
function RD.AttachCannotDirectAttack(e, c, desc, reset)
    return RushDuel.AttachSingleEffect(e, c, EFFECT_CANNOT_DIRECT_ATTACK, nil, desc, reset)
end
-- Action: Attach Cannot Select Battle Target
function RD.AttachCannotSelectBattleTarget(e, c, value, desc, reset)
    return RushDuel.AttachSingleEffect(e, c, EFFECT_CANNOT_SELECT_BATTLE_TARGET, value, desc, reset)
end
-- Action: Set Base Atk and Def
function RushDuel.SetBaseAtkDef(e, c, atk, def, reset)
    if atk ~= nil then
        RushDuel.AttachSingleEffect(e, c, EFFECT_SET_BASE_ATTACK, atk, nil, reset)
    end
    if def ~= nil and RushDuel.IsCanChangeDef(c) then
        RushDuel.AttachSingleEffect(e, c, EFFECT_SET_BASE_DEFENSE, def, nil, reset)
    end
end
-- Action: Swap Base Atk and Def
function RushDuel.SwapBaseAtkDef(e, c, reset)
    if RushDuel.IsCanChangeDef(c) then
        RushDuel.AttachSingleEffect(e, c, EFFECT_SWAP_BASE_AD, nil, nil, reset)
    end
end
-- Action: Swap Atk and Def
function RushDuel.SwapAtkDef(e, c, reset)
    if RushDuel.IsCanChangeDef(c) then
        local atk = c:GetAttack()
        local def = c:GetDefense()
        RushDuel.AttachSingleEffect(e, c, EFFECT_SET_ATTACK_FINAL, def, nil, reset)
        RushDuel.AttachSingleEffect(e, c, EFFECT_SET_DEFENSE_FINAL, atk, nil, reset)
    end
end
-- Action: Change Attribute
function RushDuel.ChangeAttribute(e, c, attribute, reset)
    return RushDuel.AttachSingleEffect(e, c, EFFECT_CHANGE_ATTRIBUTE, attribute, nil, reset)
end
-- Action: Change Position
function RushDuel.ChangePosition(target, pos)
    if pos == nil then
        return Duel.ChangePosition(target, POS_FACEUP_DEFENSE, POS_FACEUP_DEFENSE, POS_FACEUP_ATTACK, POS_FACEUP_ATTACK)
    else
        return Duel.ChangePosition(target, pos)
    end
end
-- Action: Send Opponent's Hand to Grave
function RushDuel.SendOpponentHandToGrave(tp, desc, min, max)
    local g = Duel.GetFieldGroup(tp, 0, LOCATION_HAND)
    local ct = g:GetCount()
    if ct < min then
        return 0
    end
    local ops = {}
    for i = min, math.min(max, ct), min do
        table.insert(ops, i)
    end
    local ac = 0
    if #ops == 1 then
        ac = table.remove(ops)
    elseif #ops > 1 then
        Duel.Hint(HINT_SELECTMSG, tp, desc)
        ac = Duel.AnnounceNumber(tp, table.unpack(ops))
    end
    if ac > 0 then
        local sg = g:RandomSelect(tp, ac)
        return Duel.SendtoGrave(sg, REASON_EFFECT)
    end
    return 0
end
-- Action: Send to Opponent Hand
function RushDuel.SendToOpponentHand(target)
    local g = RushDuel.ToMaximunGroup(target)
    return Duel.SendtoHand(g, nil, REASON_EFFECT)
end
-- Action: Send to Hand and Exists (Confirm To player)
function RushDuel.SendToHandAndExists(target, confirm_player, filter, count, expect)
    local g = RushDuel.ToMaximunGroup(target)
    if Duel.SendtoHand(g, nil, REASON_EFFECT) == 0 then
        return false
    end
    Duel.ConfirmCards(confirm_player, g)
    return RushDuel.IsOperatedGroupExists(filter, count, expect)
end
-- Action: Send to Deck and Exists
function RushDuel.SendToDeckAndExists(target, filter, count, expect)
    local g = RushDuel.ToMaximunGroup(target)
    return Duel.SendtoDeck(g, nil, 2, REASON_EFFECT) ~= 0 and RushDuel.IsOperatedGroupExists(filter, count, expect)
end
-- Action: Send to Opponent Deck Top (Sort)
function RushDuel.SendToOpponentDeckTop(target, player)
    local g = RushDuel.ToMaximunGroup(target)
    if g:GetCount() == 1 then
        return Duel.SendtoDeck(g, nil, 0, REASON_EFFECT)
    else
        return RushDuel.SendToDeckTop(target, player, 1 - player, true)
    end
end
-- Action: Send to Deck Top (Sort)
function RushDuel.SendToDeckTop(target, sort_player, target_player, sort)
    local g = RushDuel.ToMaximunGroup(target)
    local count = Duel.SendtoDeck(g, nil, 0, REASON_EFFECT)
    if count ~= 0 and sort then
        local ct = Duel.GetOperatedGroup():FilterCount(Card.IsLocation, nil, LOCATION_DECK)
        if ct > 1 then
            Duel.SortDecktop(sort_player, target_player, ct)
        end
    end
    return count
end
-- Action: Send to Deck Bottom (Sort)
function RushDuel.SendToDeckBottom(target, sort_player, target_player, sort)
    local g = RushDuel.ToMaximunGroup(target)
    if not sort then
        return Duel.SendtoDeck(g, nil, 1, REASON_EFFECT)
    else
        local count = Duel.SendtoDeck(g, nil, 0, REASON_EFFECT)
        if count ~= 0 then
            local ct = Duel.GetOperatedGroup():FilterCount(Card.IsLocation, nil, LOCATION_DECK)
            if ct > 1 then
                Duel.SortDecktop(sort_player, target_player, ct)
                for i = 1, ct do
                    local tc = Duel.GetDecktopGroup(target_player, 1):GetFirst()
                    Duel.MoveSequence(tc, 1)
                end
            end
        end
        return count
    end
end
-- Action: Send to Grave and Exists
function RushDuel.SendToGraveAndExists(target, filter, count, expect)
    local g = RushDuel.ToMaximunGroup(target)
    return Duel.SendtoGrave(g, REASON_EFFECT) ~= 0 and RushDuel.IsOperatedGroupExists(filter, count, expect)
end
-- Action: Send Deck Top Card(s) to Grave and Exists
function RushDuel.SendDeckTopToGraveAndExists(player, card_count, filter, count, expect)
    return Duel.DiscardDeck(player, card_count, REASON_EFFECT) ~= 0 and RushDuel.IsOperatedGroupExists(filter, count, expect)
end
